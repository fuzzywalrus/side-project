 /// <reference types="cypress" />

describe("LocalStorage test", () => {
  it("First test", () => {
    cy.visit('http://localhost:8080');
    cy.get('[cy-test="0"]').find(".card__toggleIcon").click()

    cy.get('[cy-test="0"]').find(".card__toggleIcon").invoke('attr', 'data-propertyid').as('dataId')
    cy.get('[cy-test="1"]').find(".card__toggleIcon").click().should('have.class','favorited')
    cy.get('[cy-test="1"]').find(".card__toggleIcon").click().then(() => {
      expect(localStorage.getItem('favorites')).to.equal(dataId)
    })

    cy.get('@dataId').then((dataId) => {
      console.log(dataId);
      expect(localStorage.getItem('favorites')).to.equal(dataId)
    })
  })
})
