# Getting Started

- Please read the INSTRUCTIONS.md first

# Code and Design Decisions

## Installation (... and why I changed the docker-compose)

Hi Side Developers. I figured I'd leave additional few notes about getting this project up and going.

### Node Reqs

Since Apple Silicon doesn't have a native version of Node v14, I encountered some annoyances with Libsass griping about not supporting ARM64. Rather than take the trouble of running HomeBrew in x86 flags and installing Node / NVM I elected to just run v16. I also had to npx webpack. I didn't investigate why it happened, as the end goal was to get the project working.

### ARM/64 and Docker

Docker burned a bit of time since the docker-compose had issues. First, the images not being ARM64 supported, at least in the case of MySQL. 

`no matching manifest for linux/arm64/v8 in the manifest list entries`

I didn't change it to MariaDB, which does have an ARM-compatible version, and I know from experience it works with WordPress, Drupal, etc. Instead, I used the platform flags as it'll run those using QEMU via the Linux kernel.

Once that was squared away, I came up against my next error.

`network proxy declared as external, but could not be found`

For this issue, I just hacked out the network declarations. I'm Docker capable but not any sort of expert and also killed the restart always as not necessary for this.

Interestingly, I still had errors but the containers launched even if I didn't see the `docker-compose` output on my terminal. I figured this was "good enough" as hopefully I'm not being judged on my docker-compose hacking.

### Additional changes

I added to webpack browsersync for live change reloads as I prefer to see changes live on save. Usually, I take the time to set up  live CSS injection, but it can be annoying to get going. 

## Setting up the listing page 

I decided to use PHP for the API request as this is a bit quicker than using a JS template like mustache or handlebars or going further with a heavier framework. Plus, there'd be somewhat better SEO implications, and I didn't need extended functionality. I chose to use a front-page instead of an assignable template to keep things as simple as possible.

To set up the front page, go to Settings -> Reading and set a static page as the home page.

The `front-page.php`  is bare-bones and uses `/additional-functions/property-listings.php`. The `simplyrets-curl.php` makes the API request, following the path as per the API documentation. 

This is then iterated over creating the necessary strings and quasi-templatized as a simple echo functionality to keep the business logic out of the output function `echo_propertyCard`. 

I left the credentials hardcoded into the php since they're for test data and it felt out of scope to make an `.env` file, and going through the motions of using a library like `defuse/php-encyption` and would take a bit of time as it's not my usual wheelhouse.

## Dependencies 

I elected for the framework to use bootstrap grids, a fork of Bootstrap that only provides the grids. Bootstrap makes a lot of decisions and has a large payload. You get the same lovable syntax and functionality without the bloat and you're not spending time rolling-your-own. In a project this simple it'd be totally viable as the amount of lines it'd take would be minimal.

## CSS

Since this is a relatively simple project and time-sensitive, I elected to leave all the .scss in the `style.scss` file. Normally I'd place Scss into files, in the `/assets/` folder.

I didn't use any media queries but I wanted to call that out as the absence doesn't mean I'm unfamiliar with them. I've been using them almost decade now ;) 

## JS

Much like the CSS, I chose to use a single JS file. The JS is pretty straightforward. It's vanilla ES6, as I saw that `babel-loader` was part of the webpack. On the script init, the code loads the localstorage and checks to see if any favorites have been set. 

On a mouse click event, it'll add or remove items to the favorites list. The favorite lists can also be tabbed between. The user can hit return to perform adding/removing properties from the favorites list in order to help satisfy accessibility for alternately abled users/users who prefer keyboard interaction over the mouse.

## Accessibility 

I added basic descriptions of the photos for screen readers by mashing data together, mostly to simulate what one _might_ do to assist for a page like this. I also made the favorite functionality usable without a mouse using a combo of `tabindex` and Javascript.

I didn't add any ARIA tags or vet my page in FireFox's accessibility render mode, as that'd take time. Accessibility is a sliding scale, and you can almost always "do better". 

## Page Speed

Speaking of another topic where you can always "do better", I skipped looking at Google Lighthouse analysis. I'm sure I could have improved pagespeeds by using recommended tweaks like using `font-display: swap` and it'd certainly gripe about the images. In my normal work flow, I'm always trying to balance the perils of CMS sites and the fun of time-to-paints, first meaningful renders and so on.

## Tests

I wrote a simple Cypress test that clicks on the first and second properties twice to add and remove them. Then the expected LocalStorage data is checked against the interaction. This satisfies checking to see if the javascript is functioning and the localStorage is storing the correct data. I'm new to Cypress (first time using it) but was rather impressed at how quick it was to write compared to Selenium. Regardless of what happens, I'm now incorporating Cypress into even future churn-and-burn projects.  

Note: In order to run the test, you'll need to have the webpack task manager running.


## Other options for approach

While I could have done this as a PHP plugin, it'd be interesting to have made a custom Gutenberg block that allows the user to place the listings within the Gutenberg editior. I've coded multiple custom Gutenberg blocks but it'd take me _much_ longer to develop as you're developing not just the display but also a UI that's meaningful for the editor as well.  I've never hit an API endpoint through Gutenberg or better worded, created a Gutenberg front end to code to interact with the API. 

That'd been flashy but would have required a lot of time and perhaps not the solution your team was looking for. 


Thanks for taking the time to review this. Cheers.