"use strict";

// get and show previously favorited proeprties

const savedValues = { ...localStorage };
const favorites = savedValues.favorites;
let favoritesArr = [];
if (favorites) {
    favoritesArr = favorites.split(',')
}
if (Array.isArray(favoritesArr) ) {
    favoritesArr = removeEmpties(favoritesArr);
}

    
if (favorites) {
    favoritesArr.forEach(function (favorite){
        const find = `[data-propertyid="${favorite}"]`;
        target = document.querySelector(find);
        if (target) {
            target.classList.add('favorited');
        }
    });   
}

//set click listerns
const toggleIcons = document.querySelectorAll(".card__toggleIcon");
toggleIcons.forEach( function (toggleIcon){
    toggleIcon.addEventListener( 'click', function() {
        toggleIcon.classList.toggle('favorited')
        appendRemoveFavorites(toggleIcon.dataset.propertyid);
        console.log(favoritesArr);
    });
})

//allow for accessability!
toggleIcons.forEach( function (toggleIcon){
    toggleIcon.addEventListener( 'keyup', function(event) {
        if (event.keyCode === 13) {
            toggleIcon.click();
          }
    });
})

//Add/remove from LocalStore
function appendRemoveFavorites( propertyID) {
    if (favoritesArr.includes(propertyID) ) {
        const index = favoritesArr.indexOf(propertyID)
        if (index > -1) {
            favoritesArr.splice(index, 1); // 2nd parameter means remove one 
        }
        localStorage.setItem('favorites', favoritesArr);
    } else {
        favoritesArr.push(propertyID);
        localStorage.setItem('favorites', favoritesArr);
    }
}

//init to remove erroneous entries
function removeEmpties(Arr) {
    temp = [];
    for(let i of Arr) {
        i && temp.push(i); // copy each non-empty value to the 'temp' array
    }
    return temp;
}