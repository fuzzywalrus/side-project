<?php
/**
 * The template for the front page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Side
 * @since 1.0.0
 */

get_header();



?>
<section>
    <header class="container property-listing">
        <div class="row">
            <div class="col">
                <h4 class="property-listing__title">Property Listings</h4>            
            </div>
        </div>
    </header>
    <div class="container listings">
        <div class="row">
            <?php include "additional-functions/property-listings.php"; ?>
        </div>
    </div>
</section>

<?php
get_footer();