<?php

/**
 * Performs curl to generate 
 *
 * @link https://github.com/SimplyRETS/
 *
 * @package WordPress
 * @subpackage Side
 * @since 1.0.0
 */


$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.simplyrets.com/properties",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => 
    array(
        "Authorization: Basic '" . base64_encode('simplyrets:simplyrets') . "'",
    ),
));

/**
 * 1. Execute the request 
 * 2. Check for errors 
 * 3. Close the connection 
 */
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
}

/** Decode JSON response - this is a list of listings */
$myProperties = json_decode($response);
