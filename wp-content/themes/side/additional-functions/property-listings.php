<?php
include "simplyrets-curl.php";

/* debug
echo "<pre>";
print_r($myProperties);
echo "</pre>";
*/

//template

function echo_propertyCards($myAddress = "Default Description", $img = "/images/fallback.jpg", $myPrice ="Contact Agent", $myStats = "NA", $myListDate = "Contact Agent", $myID="", $screenReader="", $count) {
   
    $html = <<<"EOT"
       <div class="col-md-4 card" cy-test="$count">
            <div class="aspect-ratio">
                <div class="content">
                    <div class="card__toggleIcon" data-propertyid="$myID" tabindex="0" >
                        <img src="/wp-content/themes/side/assets/images/heart-outline.svg" class="card__icon-outline" alt="unfavorited property">
                        <img src="/wp-content/themes/side/assets/images/heart-filled.svg" class="card__icon-filled" alt="favorited property">
                    </div>
                <img src="$img" alt="$myAddress" role="figure" aria-labelledby="caption$myID">
                <div class="caption" id="caption$myID">$screenReader</div>
                </div>
            </div>

            <p class="card__myStats">$myStats</p>
            <p class="card__myPrice">$myPrice</p>
            <p class="card__myAddress">$myAddress</p>
            <p class="card__myListDate">$myListDate</p>
       </div>
 EOT;
    echo $html;
 }

$i = 0;
$count = "test";
forEach($myProperties as $myProperty) {
    $count = strval($i);

    $i = $i + 1;
    $myAddress = $myProperty->address->streetNumberText. " " .$myProperty->address->streetName. " ".$myProperty->address->city. " ".$myProperty->address->state;
    $myImage = $myProperty->photos[0];
    $myPrice = "$".number_format($myProperty->listPrice);
    $myID = $myProperty->mlsId;

    $bedrooms = "| NA BR";
    $bathrooms = "| NA Bath";
    $area = "| NA Sq Ft";
    $myListDate = "";
    $screenReader ="House style: ".$myProperty->property->style." . Stories: ".$myProperty->property->stories.". Accessiblity: ".$myProperty->property->accessibility.".  Exterior features: ".$myProperty->property->exteriorFeatures;

    // error catching
    if ( $myProperty->property->bedrooms ) {
        $bedrooms = $myProperty->property->bedrooms." BR ";
    }
    if ( $myProperty->property->bathsFull ) {
        $calcBath = $myProperty->property->bathsFull + ($myProperty->property->bathsHalf/2);
        $bathrooms = "| ".$calcBath." Bath ";
    }
    if ($myProperty->property->area ) {
        $area = "| ".$myProperty->property->area." Sq Ft ";
    }
    if ($myProperty->listDate ) {
        $originalDate = $myProperty->listDate;
        $newDate = date("m/d/y", strtotime($originalDate));
        $myListDate = "Listed: ".$newDate ;
    }
    $myStats = $bedrooms.$bathrooms.$area;
    echo_propertyCards($myAddress, $myImage, $myPrice, $myStats, $myListDate, $myID, $screenReader, $count);
}